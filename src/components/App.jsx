import React from "react";
import PostList from "./PostList";
import "../styles/App.css";

const App = () => {
  return (
    <div className="app-container">
      <PostList />
    </div>
  );
};

export default App;
